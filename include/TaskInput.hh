#pragma once

namespace cds
{

template <typename Input>
class TaskInput
{
public:
	virtual ~TaskInput();

	virtual void recv(int src) = 0;

	virtual void send(int dst) const = 0;

private:

};

template <typename Input>
TaskInput<Input>::~TaskInput()
{  }

} /* cds */ 

