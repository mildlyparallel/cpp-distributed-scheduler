#pragma once

#include <tuple>
#include <atomic>

namespace cds {

static constexpr size_t cacheline_size = 64;

template <typename T>
T load_relaxed(const std::atomic<T> &a)
{
	return a.load(std::memory_order_relaxed);
}

template <typename T>
T load_relaxed(const std::atomic<T> *a)
{
	return a->load(std::memory_order_relaxed);
}

template <typename T>
T load_acquire(const std::atomic<T> &a)
{
	return a.load(std::memory_order_acquire);
}

template <typename T>
T load_acquire(const std::atomic<T> *a)
{
	return a->load(std::memory_order_acquire);
}

template <typename T>
T load_consume(const std::atomic<T> &a)
{
	return a.load(std::memory_order_consume);
}

template <typename T>
T load_consume(const std::atomic<T> *a)
{
	return a->load(std::memory_order_consume);
}

template <typename T>
void store_relaxed(std::atomic<T> &a, T x)
{
	return a.store(x, std::memory_order_relaxed);
}

template <typename T>
void store_relaxed(std::atomic<T> *a, T x)
{
	return a->store(x, std::memory_order_relaxed);
}

template <typename T>
void store_release(std::atomic<T> &a, T x)
{
	return a.store(x, std::memory_order_release);
}

template <typename T>
void store_release(std::atomic<T> *a, T x)
{
	return a->store(x, std::memory_order_release);
}

template <typename T>
bool cas_weak_seq_cst(std::atomic<T> &a, T &expected, T desired)
{
	return a.compare_exchange_weak(expected, desired, std::memory_order_seq_cst, std::memory_order_relaxed);
}

template <typename T>
bool cas_weak_seq_cst(std::atomic<T> *a, T &expected, T desired)
{
	return a->compare_exchange_weak(expected, desired, std::memory_order_seq_cst, std::memory_order_relaxed);
}

template <typename T>
bool cas_weak_release(std::atomic<T> &a, T &expected, T desired)
{
	return a.compare_exchange_weak(expected, desired, std::memory_order_release, std::memory_order_relaxed);
}

template <typename T>
bool cas_weak_release(std::atomic<T> *a, T &expected, T desired)
{
	return a->compare_exchange_weak(expected, desired, std::memory_order_release, std::memory_order_relaxed);
}

template <typename T>
bool cas_strong_seq_cst(std::atomic<T> &a, T &expected, T desired)
{
	return a.compare_exchange_strong(expected, desired, std::memory_order_seq_cst, std::memory_order_relaxed);
}

template <typename T>
bool cas_strong_seq_cst(std::atomic<T> *a, T &expected, T desired)
{
	return a->compare_exchange_strong(expected, desired, std::memory_order_seq_cst, std::memory_order_relaxed);
}

template <typename T>
bool cas_strong_release(std::atomic<T> &a, T &expected, T desired)
{
	return a.compare_exchange_strong(expected, desired, std::memory_order_release, std::memory_order_relaxed);
}

template <typename T>
bool cas_strong_release(std::atomic<T> *a, T &expected, T desired)
{
	return a->compare_exchange_strong(expected, desired, std::memory_order_release, std::memory_order_relaxed);
}

template <typename T>
T dec_relaxed(std::atomic<T> &a, T v = 1)
{
	return a.fetch_sub(v, std::memory_order_relaxed);
}

template <typename T>
T dec_relaxed(std::atomic<T> *a, T v = 1)
{
	return a->fetch_sub(v, std::memory_order_relaxed);
}

template <typename T>
T dec_release(std::atomic<T> &a, T v = 1)
{
	return a.fetch_sub(v, std::memory_order_release);
}

template <typename T>
T dec_release(std::atomic<T> *a, T v = 1)
{
	return a->fetch_sub(v, std::memory_order_release);
}

template <typename T>
T inc_relaxed(std::atomic<T> &a, T v = 1)
{
	return a.fetch_add(v, std::memory_order_relaxed);
}

template <typename T>
T inc_relaxed(std::atomic<T> *a, T v = 1)
{
	return a->fetch_add(v, std::memory_order_relaxed);
}

template <typename T>
T inc_release(std::atomic<T> &a, T v = 1)
{
	return a.fetch_add(v, std::memory_order_release);
}

template <typename T>
T inc_release(std::atomic<T> *a, T v = 1)
{
	return a->fetch_add(v, std::memory_order_release);
}

inline
void fence_release()
{
	std::atomic_thread_fence(std::memory_order_release);
}

inline
void fence_acquire()
{
	std::atomic_thread_fence(std::memory_order_acquire);
}

inline
void fence_seq_cst()
{
	std::atomic_thread_fence(std::memory_order_seq_cst);
}

} /* namespace cds */
