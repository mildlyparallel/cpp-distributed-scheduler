#pragma once

#include <atomic>
#include <memory>
#include <cassert>

#include "utils.hh"

namespace cds {

template <typename T>
class Deque
{
public:
	Deque();

	Deque(const Deque &other);

	Deque(Deque &&other);

	virtual ~Deque();

	void put(const T &task);

	bool take(T &task);

	bool steal(T &task);

	size_t size() const;

	size_t capacity() const;

private:
	static constexpr size_t m_init_log_size = 8;

	struct MaskedArray {
		size_t mask;

		T *data;

		T &operator[](size_t i) {
			assert(data);
			assert(mask);
			assert((i & mask) < capacity());
			return data[i & mask];
		}

		const T &operator[](size_t i) const {
			assert(data);
			assert(mask);
			assert((i & mask) < capacity());
			return data[i & mask];
		}

		size_t capacity() const {
			if (mask)
				return mask + 1;
			return 0;
		}
	};

	MaskedArray grow(size_t log_size);

	size_t m_log_size;

	alignas(cacheline_size) std::atomic_size_t m_top;

	alignas(cacheline_size) std::atomic_size_t m_bottom;

	alignas(cacheline_size) std::atomic< MaskedArray > m_data;
};

template<typename T>
Deque<T>::Deque()
: m_top(1)
, m_bottom(1)
{
	size_t sz = 1 << m_init_log_size;
	MaskedArray d;
	d.mask = sz - 1;
	d.data = new T[sz]();

	// XXX: valgrind says Conditional jump or move depends on uninitialised value(s)
	// for this store. 
	// Uninitialised value was created by a heap allocation
	// When Worker was created in Scheduler
	m_data.store(d);

	m_log_size = m_init_log_size;
}

template<typename T>
Deque<T>::Deque(const Deque &other)
: m_log_size(other.m_log_size)
{
	m_top.store(other.m_top.load());
	m_bottom.store(other.m_bottom.load());

	size_t sz = 1 << m_log_size;
	MaskedArray d;
	d.mask = sz - 1;
	d.data = new T[sz]();

	auto otherdata = other.m_data.load();
	for (size_t i = m_top; i < m_bottom; i++)
		d[i] = otherdata.data[i];

	m_data.store(d);
}

template<typename T>
Deque<T>::Deque(Deque &&other)
: m_log_size(other.m_log_size)
{
	m_top.store(other.m_top.load());
	m_bottom.store(other.m_bottom.load());
	m_data.store(other.m_data.load());

	other.m_log_size = 0;
	other.m_top = 1;
	other.m_bottom = 1;
	other.m_data.store({0, nullptr});
}

template<typename T>
Deque<T>::~Deque()
{ 
	auto data = m_data.load();
	if (data.data)
		delete []data.data;
}

template<typename T>
void Deque<T>::put(const T &task)
{
	size_t b = load_relaxed(m_bottom);
	size_t t = load_acquire(m_top);
	auto data = load_relaxed(m_data);

	if (b - t > data.capacity() - 1)
		data = grow(m_log_size + 1);

	data[b] = task;

	store_relaxed(m_bottom, b + 1);

	fence_release();
}

template<typename T>
bool Deque<T>::take(T &task)
{
	size_t b = dec_relaxed(m_bottom) - 1;
	size_t t = load_acquire(m_top);
	auto data = load_relaxed(m_data);

	// Deque<T> was empty, restoring to empty state
	if (t > b) {
		store_relaxed(m_bottom, b + 1);
		return false;
	}

	task = data[b];

	if (t == b) {
		// Check if they are not stollen
		if (!cas_strong_release(m_top, t, t + 1)) {
			store_relaxed(m_bottom, b + 1);
			return false;
		}

		store_relaxed(m_bottom, b + 1);
	}

	return true;
}

template<typename T>
bool Deque<T>::steal(T &task)
{
	fence_acquire();

	size_t b = load_relaxed(m_bottom);
	size_t t = load_relaxed(m_top);
	auto data = load_relaxed(m_data);

	// Deque is empty
	if (t >= b)
		return false;

	task = data[t];

	if (!cas_weak_release(m_top, t, t + 1))
		return false;

	return true;
}

template<typename T>
size_t Deque<T>::capacity() const
{
	auto data = m_data.load();
	return data.capacity();
}

template<typename T>
size_t Deque<T>::size() const
{
	size_t t = m_top.load();
	size_t b = m_bottom.load();
	if (t >= b)
		return 0;
	return b - t;
}

template<typename T>
typename Deque<T>::MaskedArray Deque<T>::grow(size_t log_size)
{
	MaskedArray new_data;

	size_t sz = 1 << log_size;
	new_data.mask = sz - 1;
	new_data.data = new T[sz]();

	size_t t = load_relaxed(m_top);
	size_t b = load_relaxed(m_bottom);

	MaskedArray old_data = load_relaxed(m_data);

	assert(new_data.capacity() > old_data.capacity());
	for (size_t i = t; i < b; i++)
		new_data[i] = old_data[i];

	store_relaxed(m_data, new_data);

	m_log_size = log_size;

	assert(old_data.data);
	delete []old_data.data;

	return new_data;
}

} /* namespace cds */
