#pragma once

#include <cstddef>
#include <thread>
#include <atomic>

#include "Mpi.hh"
#include "Dispatcher.hh"

namespace cds {

template <typename Input, typename Output>
class Scheduler
{
public:
	Scheduler();

	virtual ~Scheduler();

	size_t get_nr_procs() const;

	size_t get_proc_id() const;

	void set_nr_threads(size_t nr_threads);

	size_t get_nr_threads() const;

	void set_mpi_comm(MPI_Comm comm);

	MPI_Comm get_mpi_comm();

	template<typename... Inargs>
	void spawn(Output *output, Inargs... input);

	void sync();

	void start();

	void stop();

protected:

private:
	void configure();

	size_t m_proc_id;
	size_t m_nr_procs;
	size_t m_nr_threads;

	Mpi m_mpi;

	std::vector< Worker<Input, Output> *> m_workers;
};

template<typename Input, typename Output>
Scheduler<Input, Output>::Scheduler()
: m_proc_id(0)
, m_nr_procs(0)
, m_nr_threads(0)
{  }

template<typename Input, typename Output>
Scheduler<Input, Output>::~Scheduler()
{  }

template<typename Input, typename Output>
size_t Scheduler<Input, Output>::get_nr_procs() const
{
	return m_nr_procs;
}

template<typename Input, typename Output>
size_t Scheduler<Input, Output>::get_proc_id() const
{
	return m_proc_id;
}

template<typename Input, typename Output>
void Scheduler<Input, Output>::set_nr_threads(size_t nr_threads)
{
	m_nr_threads = nr_threads;
}

template<typename Input, typename Output>
size_t Scheduler<Input, Output>::get_nr_threads() const
{
	return m_nr_threads;
}

template<typename Input, typename Output>
void Scheduler<Input, Output>::set_mpi_comm(MPI_Comm comm)
{
	m_mpi.set_comm(comm);
}

template<typename Input, typename Output>
MPI_Comm Scheduler<Input, Output>::get_mpi_comm()
{
	return m_mpi.get_comm();
}

template<typename Input, typename Output>
void Scheduler<Input, Output>::configure()
{
	if (m_nr_threads == 0)
		m_nr_threads = std::thread::hardware_concurrency();

	if (!m_mpi.is_set()) {
		m_nr_procs = 0;
		m_proc_id = 0;
		return;
	}

	m_nr_procs = m_mpi.get_nr_procs();
	m_proc_id = m_mpi.get_proc_id();

	if (m_nr_procs <= 1) {
		m_nr_procs = 0;
		m_proc_id = 0;
		return;
	}

	m_nr_threads++;

	// if (m_nr_threads == 1)
	// 	m_nr_threads = 2;
}

template<typename Input, typename Output>
void Scheduler<Input, Output>::start()
{
	configure();

	assert(m_workers.empty());

	for (size_t i = 0; i < m_nr_threads; ++i) {
		Worker<Input, Output> *w;
		if (i == 0 && m_nr_procs > 1) {
			auto d = new Dispatcher<Input, Output>();
			d->set_mpi_comm(m_mpi);
			w = d;
		} else {
			w = new Worker<Input, Output>();
		}

		w->set_id(i);
		m_workers.push_back(w);
	}

	for (size_t i = 0; i < m_nr_threads; ++i) {
		for (size_t j = 0; j < m_nr_threads; ++j) {
			if (j != i)
				m_workers[i]->add_victim(m_workers[j]);
		}
	}

	for (size_t i = 1; i < m_nr_threads; ++i)
		m_workers[i]->start();

	for (size_t i = 1; i < m_nr_threads; ++i) {
		while (!m_workers[i]->is_ready())
			std::this_thread::yield();
	}
}

template<typename Input, typename Output>
void Scheduler<Input, Output>::stop()
{
	for (size_t i = 1; i < m_nr_threads; ++i) {
		m_workers[i]->stop();
	}

	std::cerr << "Process id: " << m_proc_id << " of " << m_nr_procs << std::endl;

	std::string outfile = "total-stats-";
	outfile += std::to_string(m_proc_id);
	outfile += ".csv";

	std::ofstream out(outfile);
	m_workers[0]->get_counter().print_header(out);

	m_workers[0]->get_counter().print_header();
	for (size_t i = 0; i < m_nr_threads; ++i) {
		m_workers[i]->get_counter().print_row(i);
		m_workers[i]->get_counter().print_row(out, i);
	}
}

template<typename Input, typename Output>
template<typename... Inargs>
void Scheduler<Input, Output>::spawn(Output *output, Inargs... args)
{
	assert(!m_workers.empty());
	m_workers.front()->spawn(output, args...);
}

template<typename Input, typename Output>
void Scheduler<Input, Output>::sync()
{
	assert(!m_workers.empty());
	return m_workers.front()->sync();
}

} /* namespace cds */
