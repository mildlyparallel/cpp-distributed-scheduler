#pragma once

#include <cstdint>

namespace cds {

inline uint32_t xorshift_rand() {
	static thread_local uint32_t x = 2463534242;

	x ^= x >> 13;
	x ^= x << 17;
	x ^= x >> 5;
	return x;
}


} /* namespace cds */
