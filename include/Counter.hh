#pragma once

#include <iostream>
#include <iomanip>

namespace cds {

class Counter
{
public:
	Counter();

	virtual ~Counter();

	enum Event { 
		Put = 0,
		Take,
		ThiefSteal,
		OwnerSteal,
		RequestSend,
		RequestRecv,
		ResponseSuccSend,
		ResponseSuccRecv,
		ResponseFailSend,
		ResponseFailRecv,

		nr_events
	};

	void count(Event e);

	static void print_header();

	void print_row(size_t w) const;

	void print_header(std::ofstream &out) const;

	void print_row(std::ofstream &out, size_t w) const;

private:
	static constexpr size_t m_field_width = 8;

	static const constexpr char *m_events[nr_events] = {
		"Put",
		"Take",
		"TSteal",
		"OSteal",
		"ReqS",
		"ReqR",
		"ResOkS",
		"ResOkR",
		"ResNoS",
		"ResNoR",
	};

	std::array<unsigned long long, nr_events> m_counters;
};

const constexpr char *Counter::m_events[Counter::nr_events];

Counter::Counter()
{ 
	std::fill(m_counters.begin(), m_counters.end(), 0);
}

Counter::~Counter()
{  }


void Counter::count(Counter::Event e)
{
	m_counters[static_cast<size_t>(e)]++;
}

void Counter::print_header(std::ofstream &out) const
{
	out << "worker";
	for (auto h : m_events)
		out << "," << h;
	out << "\n";
}

void Counter::print_row(std::ofstream &out, size_t w) const
{
	out << w;
	for (size_t i = 0; i < nr_events; ++i)
		out << "," << m_counters[i];
	out << "\n";
}

void Counter::print_header()
{
	std::cerr << "t# |";
	for (auto h : m_events)
		std::cerr << std::setw(m_field_width) << h << " |";
	std::cerr << std::endl;
}

void Counter::print_row(size_t w) const
{
	std::cerr << std::setw(2) << w << " |";

	for (size_t i = 0; i < nr_events; ++i)
		std::cerr << std::setw(m_field_width) << m_counters[i] << " |";

	std::cerr << std::endl;
}

} /* namespace cds */
