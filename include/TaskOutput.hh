#pragma once

#include <atomic>

namespace cds
{

template <typename Input, typename Output>
class Worker;

template <typename Input, typename Output>
class TaskOutput
{
public:
	virtual ~TaskOutput();

	virtual void recv(int src) = 0;

	virtual void send(int dst) const = 0;

	virtual void run(Worker<Input, Output> &worker, const Input &input) = 0;

private:

};

template <typename Input, typename Output>
TaskOutput<Input, Output>::~TaskOutput()
{  }

} /* cds */ 

