#pragma once

#include <mpi.h>
#include <cassert>

namespace cds {

class Mpi {
public:
	Mpi()
	: m_comm_set(false)
	, m_nr_procs(0)
	, m_proc_id(0)
	{ }

	Mpi(MPI_Comm comm)
	: m_comm(comm)
	, m_comm_set(true)
	, m_nr_procs(0)
	, m_proc_id(0)
	{
		read_cfg();
	}

	~Mpi()
	{ }

	void set_comm(MPI_Comm comm)
	{
		m_comm = comm;
		m_comm_set = true;
		read_cfg();
	}

	MPI_Comm get_comm() const
	{
		return m_comm;
	}

	bool is_set() const
	{
		return m_comm_set;
	}

	int get_nr_procs() const
	{
		return m_nr_procs;
	}

	int get_proc_id() const
	{
		return m_proc_id;
	}

	bool iprobe(int src)
	{
		assert(m_comm_set);
		int flag = 0;
		MPI_Iprobe(src, 0, m_comm, &flag, MPI_STATUS_IGNORE);
		return flag;
	}

	std::tuple<bool, int> iprobe()
	{
		assert(m_comm_set);
		int flag = 0;
		MPI_Status st;
		MPI_Iprobe(MPI_ANY_SOURCE, 0, m_comm, &flag, &st);

		return {flag == 1, st.MPI_SOURCE};
	}

	void send(int dst, uint8_t n)
	{
		assert(m_comm_set);
		MPI_Send(&n, 1, MPI_UNSIGNED_CHAR, dst, 0, m_comm);
	}

	int recv(uint8_t &n)
	{
		assert(m_comm_set);
		MPI_Status st;
		MPI_Recv(&n, 1, MPI_UNSIGNED_CHAR, MPI_ANY_SOURCE, 0, m_comm, &st);
		return st.MPI_SOURCE;
	}

	void recv(int src, uint8_t &n)
	{
		assert(m_comm_set);
		MPI_Recv(&n, 1, MPI_UNSIGNED_CHAR, src, 0, m_comm, MPI_STATUS_IGNORE);
	}

	void send(int dst, void *ptr)
	{
		assert(m_comm_set);

		// TODO: store in buffer of sizeof(void*) size or something
		MPI_Send(&ptr, 1, MPI_UNSIGNED_LONG_LONG, dst, 0, m_comm);
	}

	void recv(int src, void *&ptr)
	{
		assert(m_comm_set);

		uint64_t p;

		MPI_Recv(&p, 1, MPI_UNSIGNED_LONG_LONG, src, 0, m_comm, MPI_STATUS_IGNORE);

		ptr = reinterpret_cast<void *>(p);
	}

private:
	void read_cfg()
	{
		assert(m_comm_set);
		MPI_Comm_size(m_comm, &m_nr_procs);
		MPI_Comm_rank(m_comm, &m_proc_id);
	}

	MPI_Comm m_comm;

	bool m_comm_set;

	int m_nr_procs;
	int m_proc_id;
};

} /* namespace cds */
