#pragma once

#include <vector>
#include <thread>
#include <atomic>
#include <cassert>
#include <iostream>

#include "Deque.hh"
#include "Random.hh"
#include "Counter.hh"
#include "utils.hh"

namespace cds {

#define CDS_COUNT(e) m_counters.count(Counter::Event::e)

template <typename Input, typename Output>
class Worker
{
public:
	Worker();

	virtual ~Worker();

	void set_id(size_t id);

	size_t get_id() const;

	bool is_ready() const;

	void start();

	void stop();

	template <typename... In>
	void spawn(Output *output, In... input);

	virtual void sync();

	void dispatch_loop(Mpi mpi);

	void master_dispatch_loop(Mpi mpi);

	void add_victim(Worker<Input, Output> *victim);

	const Counter &get_counter() const;

// protected:
	struct Task {
		std::atomic_size_t *nr_spawned_siblings;
		Output *output;
		Input input;
	};

	struct RTStats {
		RTStats()
		: nr_taken(0)
		, nr_stolen(0)
		, nr_steals(0)
		, nr_put(0)
		, nr_take_fails(0)
		{  }

		uint64_t nr_taken;
		uint64_t nr_stolen;
		uint64_t nr_steals;
		uint64_t nr_put;
		uint64_t nr_take_fails;
	};

	size_t m_id;

	std::vector< Worker<Input, Output> *> m_victims;

	Deque<Task> m_deque;

	std::atomic_size_t m_nr_spawned_roots;

	std::atomic_size_t *m_nr_spawned_current;

	Counter m_counters;

	bool steal_task(Task &task);

	RTStats m_stats;

private:
	void steal_loop();

	int find_victim_node(const Mpi &mpi) const;

	bool should_request() const;

	bool steal_and_send(Mpi mpi, int dst);

	void recv_task(Mpi mpi, int src);

	void check_requested(Mpi mpi);

	void recieve_completed(Mpi mpi, int src);

	uint8_t handle_incoming(Mpi mpi, int src);

	std::thread m_thread;

	std::atomic_bool m_ready;

	std::atomic_bool m_stopped;

};

template <typename Input, typename Output>
Worker<Input, Output>::Worker()
: m_id(0)
, m_nr_spawned_roots(0)
, m_nr_spawned_current(&m_nr_spawned_roots)
, m_ready(false)
, m_stopped(false)
{ }

template <typename Input, typename Output>
Worker<Input, Output>::~Worker()
{ 
	stop();
}

template <typename Input, typename Output>
void Worker<Input, Output>::set_id(size_t id)
{
	m_id = id;
}

template <typename Input, typename Output>
size_t Worker<Input, Output>::get_id() const
{
	return m_id;
}

template <typename Input, typename Output>
void Worker<Input, Output>::start()
{
	m_thread = std::thread([=, this] {
		steal_loop();
	});
}

template <typename Input, typename Output>
void Worker<Input, Output>::stop()
{
	m_stopped.store(true);

	if (m_thread.joinable())
		m_thread.join();
}

template <typename Input, typename Output>
void Worker<Input, Output>::steal_loop()
{
	m_ready.store(true);

	while (!load_relaxed(m_stopped)) {
		Task task;
		bool have_task = steal_task(task);

		if (!have_task) {
			std::this_thread::yield();
			continue;
		}

		m_stats.nr_stolen++;

		CDS_COUNT(ThiefSteal);

		assert(task.output);
		assert(task.nr_spawned_siblings);

		std::atomic_size_t nr_spawned_children(0);
		m_nr_spawned_current = &nr_spawned_children;

		task.output->run(*this, task.input);

		assert(nr_spawned_children == 0);

		dec_release(task.nr_spawned_siblings);
	}
}

template <typename Input, typename Output>
bool Worker<Input, Output>::is_ready() const
{
	return load_relaxed(m_ready);
}

template <typename Input, typename Output>
template <typename... In>
void Worker<Input, Output>::spawn(Output *output, In... args)
{
	assert(m_nr_spawned_current);

	inc_relaxed(m_nr_spawned_current);

	Task task {
		.nr_spawned_siblings = m_nr_spawned_current,
		.output = output,
		.input  = {args...}
	};

	m_stats.nr_put++;

	m_deque.put(task);

	CDS_COUNT(Put);
}

template <typename Input, typename Output>
void Worker<Input, Output>::add_victim(Worker<Input, Output> *victim)
{
	m_victims.push_back(victim);
}

template <typename Input, typename Output>
bool Worker<Input, Output>::steal_task(Worker::Task &task)
{
	assert(!m_victims.empty());

	m_stats.nr_steals++;

	size_t v = xorshift_rand() % m_victims.size();

	assert(m_victims[v] != this);

	return m_victims[v]->m_deque.steal(task);
}

template <typename Input, typename Output>
void Worker<Input, Output>::sync()
{
	Task task;
	bool have_task = false;

	auto nr_spawned_current = m_nr_spawned_current;

	while (true) {
		if (have_task) {
			assert(task.output);
			assert(task.nr_spawned_siblings);

			CDS_COUNT(Take);

			std::atomic_size_t nr_spawned_children(0);
			m_nr_spawned_current = &nr_spawned_children;

			task.output->run(*this, task.input);

			assert(nr_spawned_children == 0);

			assert(task.nr_spawned_siblings->load() > 0);
			// TODO: no need to release when the 'task' was taken, not stolen
			dec_release(task.nr_spawned_siblings);

			m_nr_spawned_current = nr_spawned_current;
		}

		if (load_acquire(nr_spawned_current) == 0)
			break;

		have_task = m_deque.take(task);

		if (have_task) {
			m_stats.nr_taken++;
			continue;
		} else {
			m_stats.nr_take_fails++;
		}

		have_task = steal_task(task);

		if (!have_task)
			std::this_thread::yield();
		else
			m_stats.nr_stolen++;

		CDS_COUNT(OwnerSteal);
	}
}

template <typename Input, typename Output>
const Counter &Worker<Input, Output>::get_counter() const
{
	return m_counters;
}

#undef CDS_COUNT

} /* namespace cds */
