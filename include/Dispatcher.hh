#pragma once

#include <vector>
#include <thread>
#include <atomic>
#include <cassert>
#include <fstream>
#include <iostream>

#include "Mpi.hh"
#include "Worker.hh"
#include "utils.hh"

#include "config.h"

namespace cds {

#define CDS_COUNT(e) this->m_counters.count(Counter::Event::e)

template <typename Input, typename Output>
class Dispatcher : public Worker<Input, Output>
{
public:
	Dispatcher();

	virtual ~Dispatcher();

	virtual void sync();

	void set_mpi_comm(const Mpi &mpi);

protected:

private:
	void dispatch_loop();

	void master_dispatch_loop();

	int find_victim_node() const;

	bool should_request() const;

	bool steal_and_send(int dst);

	void recv_task(int src);

	void check_requested();

	void recieve_completed(int src);

	uint8_t handle_incoming(int src);

	void gather_stats();

	void write_stats();

	struct RequestedTask {
		RequestedTask()
		: source(-1)
		{  };

		int source;
		std::atomic_size_t nr_spawned_stub;
		void *source_nr_spawned;
		void *source_output;
		Output output;
	};

	enum Message : uint8_t {
		Done = 0,
		Request,
		ResponseSucc,
		ResponseFail,
		Complete,

		nr_messages
	};

	Mpi m_mpi;

	MPI_Comm m_mpi_comm;

	int m_mpi_rank;

	size_t m_nr_requested;

	double m_steal_ratio;

	size_t m_last_nr_steals;
	size_t m_last_nr_stolen;

	static constexpr double m_steal_ratio_tres = STEAL_THRES;

	std::array<RequestedTask, REQ_BUF_SIZE> m_requested_tasks;

	std::chrono::time_point<std::chrono::high_resolution_clock> m_start_tp;
	std::chrono::time_point<std::chrono::high_resolution_clock> m_last_sample_tp;
	std::chrono::time_point<std::chrono::high_resolution_clock> m_last_trace_tp;

	std::vector<std::tuple<uint64_t, typename Worker<Input, Output>::RTStats>> m_stats_totals;
};

template <typename Input, typename Output>
Dispatcher<Input, Output>::Dispatcher()
: m_nr_requested(0)
, m_last_nr_steals(0)
, m_last_nr_stolen(0)
{
	for (auto &r : m_requested_tasks) {
		r.source = -1;
	}
}

template <typename Input, typename Output>
Dispatcher<Input, Output>::~Dispatcher()
{ }

template <typename Input, typename Output>
bool Dispatcher<Input, Output>::should_request() const
{
	// return false;
	if ((m_nr_requested + 1) >= m_requested_tasks.size())
		return false;

	if (m_steal_ratio < m_steal_ratio_tres)
		return false;
	// std::cerr << "m_steal_ratio = " << m_steal_ratio << std::endl;

	return true;
}

template <typename Input, typename Output>
int Dispatcher<Input, Output>::find_victim_node() const
{
	int v = xorshift_rand() % m_mpi.get_nr_procs();

	if (v == m_mpi.get_proc_id()) {
		v++;
		if (v == m_mpi.get_nr_procs())
			v = 0;
	}

	return v;
}

template <typename Input, typename Output>
bool Dispatcher<Input, Output>::steal_and_send(int dst)
{

	if (m_steal_ratio > m_steal_ratio_tres) {
		CDS_COUNT(ResponseFailSend);
		m_mpi.send(dst, Message::ResponseFail);
		return false;
	}

	typename Worker<Input,Output>::Task task;
	bool have_task = this->steal_task(task);

	if (!have_task) {
		CDS_COUNT(ResponseFailSend);
		m_mpi.send(dst, Message::ResponseFail);
		return false;
	}

	CDS_COUNT(ResponseSuccSend);
	m_mpi.send(dst, Message::ResponseSucc);
	m_mpi.send(dst, reinterpret_cast<void*>(task.nr_spawned_siblings));
	m_mpi.send(dst, reinterpret_cast<void*>(task.output));
	task.input.send(dst);

	return true;
}

template <typename Input, typename Output>
void Dispatcher<Input, Output>::recv_task(int src)
{
	assert(src >= 0);

	size_t req_pos = m_requested_tasks.size();

	for (size_t i = 0; i < m_requested_tasks.size(); ++i) {
		if (m_requested_tasks[i].source >= 0)
			continue;
		req_pos = i;
		break;
	}

	assert(req_pos < m_requested_tasks.size());

	auto &req = m_requested_tasks[req_pos];

	req.source = src;
	req.nr_spawned_stub = 1;
	req.output = Output();

	typename Worker<Input,Output>::Task task;
	m_mpi.recv(src, req.source_nr_spawned);
	m_mpi.recv(src, req.source_output);
	task.input.recv(src);
	task.nr_spawned_siblings = &req.nr_spawned_stub;
	task.output = &req.output;

	this->m_deque.put(task);

	CDS_COUNT(Put);
}

template <typename Input, typename Output>
void Dispatcher<Input, Output>::check_requested()
{
	for (size_t i = 0; i < m_requested_tasks.size(); ++i) {
		if (m_requested_tasks[i].source < 0)
			continue;

		auto &req = m_requested_tasks[i];
		if (req.nr_spawned_stub.load() == 1)
			continue;

		m_mpi.send(req.source, Message::Complete);
		m_mpi.send(req.source, req.source_nr_spawned);
		m_mpi.send(req.source, req.source_output);
		req.output.send(req.source);

		m_requested_tasks[i].source = -1;

		assert(m_nr_requested > 0);
		m_nr_requested--;
	}
}

template <typename Input, typename Output>
void Dispatcher<Input, Output>::recieve_completed(int src)
{
	void *p = nullptr;
	m_mpi.recv(src, p);

	void *o = nullptr;
	m_mpi.recv(src, o);

	Output *po = reinterpret_cast<Output *>(o);
	po->recv(src);

	std::atomic_size_t *pn = reinterpret_cast<std::atomic_size_t *>(p);
	dec_release(pn);
}

template <typename Input, typename Output>
uint8_t Dispatcher<Input, Output>::handle_incoming(int src)
{
	uint8_t message;

	m_mpi.recv(src, message);

	if (message == Message::Request) {
		CDS_COUNT(RequestRecv);
		steal_and_send(src);
	} else if (message == Message::Complete) {
		recieve_completed(src);
	} else if (message == Message::ResponseSucc) {
		recv_task(src);
		CDS_COUNT(ResponseSuccRecv);
	} else if (message == Message::ResponseFail) {
		assert(m_nr_requested > 0);
		m_nr_requested--;
		CDS_COUNT(ResponseFailRecv);
	}

	return message;
}

template <typename Input, typename Output>
void Dispatcher<Input, Output>::dispatch_loop()
{
	while (true) {
		check_requested();

		auto [ have, src ] = m_mpi.iprobe();

		if (have) {
			uint8_t message = handle_incoming(src);

			if (message == Message::Done)
				break;
		}

		gather_stats();

		if (!should_request()) {
			std::this_thread::yield();
			continue;
		}

		int v = find_victim_node();
		m_mpi.send(v, Message::Request);
		CDS_COUNT(RequestSend);

		m_nr_requested++;
	}

	write_stats();
}

template <typename Input, typename Output>
void Dispatcher<Input, Output>::master_dispatch_loop()
{
	assert(m_mpi.get_nr_procs() >= 2);
	assert(m_mpi.get_proc_id() == 0);

	while (this->m_nr_spawned_current->load() != 0) {
		check_requested();

		auto [ have, src ] = m_mpi.iprobe();

		if (have)
			handle_incoming(src);

		gather_stats();

		if (!should_request()) {
			std::this_thread::yield();
			continue;
		}

		int v = find_victim_node();
		m_mpi.send(v, Message::Request);
		CDS_COUNT(RequestSend);

		m_nr_requested++;
	}

	for (int i = 1; i < m_mpi.get_nr_procs(); ++i)
		m_mpi.send(i, Message::Done);

	write_stats();
}

template <typename Input, typename Output>
void Dispatcher<Input, Output>::sync()
{
	m_start_tp = std::chrono::high_resolution_clock::now();

	if (m_mpi.get_proc_id() == 0) {
		master_dispatch_loop();
	} else {
		dispatch_loop();
	}
}

template <typename Input, typename Output>
void Dispatcher<Input, Output>::set_mpi_comm(const Mpi &mpi)
{
	m_mpi = mpi;
}

template <typename Input, typename Output>
void Dispatcher<Input, Output>::gather_stats()
{
	auto tp = std::chrono::high_resolution_clock::now();

	uint64_t dt_last_sample = std::chrono::duration_cast<std::chrono::microseconds>(tp - m_last_sample_tp).count();

	if (dt_last_sample >= SAMPLING_RATE_US) {
		m_last_sample_tp = tp;

		size_t nr_steals = 0;
		size_t nr_stolen = 0;

		for (auto &w : this->m_victims) {
			nr_steals += w->m_stats.nr_steals;
			nr_stolen += w->m_stats.nr_stolen;
		}

		uint64_t d_steals = nr_steals - m_last_nr_steals;
		uint64_t d_stolen = nr_stolen - m_last_nr_stolen;

		m_last_nr_steals = nr_steals;
		m_last_nr_stolen = nr_stolen;

		if (d_steals == 0)
			m_steal_ratio = 0;
		else
			m_steal_ratio = 1. - 1. * d_stolen / d_steals;
	}

#if ENABLE_TRACING
	uint64_t dt_last_trace = std::chrono::duration_cast<std::chrono::microseconds>(tp - m_last_trace_tp).count();

	if (dt_last_trace >= SAMPLING_RATE_TRACE_US) {
		m_last_trace_tp = tp;

		typename Worker<Input,Output>::RTStats totals;

		for (auto &w : this->m_victims) {
			totals.nr_steals += w->m_stats.nr_steals;
			totals.nr_stolen += w->m_stats.nr_stolen;
			totals.nr_taken += w->m_stats.nr_taken;
			totals.nr_put += w->m_stats.nr_put;
			totals.nr_take_fails += w->m_stats.nr_take_fails;
		}

		uint64_t dt_from_start = std::chrono::duration_cast<std::chrono::microseconds>(tp - m_start_tp).count();
		m_stats_totals.push_back({dt_from_start, totals});
	}
#endif
}

template <typename Input, typename Output>
void Dispatcher<Input, Output>::write_stats()
{
	std::string filename = "stats-";
	filename += std::to_string(m_mpi.get_proc_id());
	filename += ".csv";

	std::ofstream out(filename);
	out << "t,dt,steals,stolen,taken,put,take_failed\n";

	if (m_stats_totals.empty())
		return;

	auto &[tp, sp] = m_stats_totals.front();;

	for (auto &[t, s] : m_stats_totals) {
		if ((t < tp) ||
			(s.nr_stolen < sp.nr_stolen) || 
			(s.nr_taken < sp.nr_taken) || 
			(s.nr_steals < sp.nr_steals) ||
			(s.nr_put < sp.nr_put) ||
			(s.nr_take_fails < sp.nr_take_fails)
		) {
			continue;
		}

		uint64_t dt = t - tp;
		uint64_t steals = s.nr_steals - sp.nr_steals;
		uint64_t stolen = s.nr_stolen - sp.nr_stolen;
		uint64_t taken = s.nr_taken - sp.nr_taken;
		uint64_t put = s.nr_put - sp.nr_put;
		uint64_t take_failed = s.nr_take_fails - sp.nr_take_fails;

		out << t << ",";
		out << dt << ",";
		out << steals << ",";
		out << stolen << ",";
		out << taken << ",";
		out << put << ",";
		out << take_failed << "\n";

		tp = t;
		sp = s;
	}
}

#undef CDS_COUNT

} /* namespace cds */
