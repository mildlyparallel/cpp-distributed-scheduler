#include "gtest/gtest.h"

#include <thread>

#include "Deque.hh"

using namespace ::testing;

TEST(DequeTest, default_ctor__size_is_0) {
	cds::Deque<int> d;
	EXPECT_EQ(d.size(), 0);

	int t;
	ASSERT_FALSE(d.take(t));
	ASSERT_FALSE(d.steal(t));
	ASSERT_EQ(d.size(), 0);
}

TEST(DequeTest, default_ctor__capacity_gt_0) {
	cds::Deque<int> d;
	EXPECT_GT(d.capacity(), 0);
}

TEST(DequeTest, put_take__single_no_resize) {
	size_t ntasks = 8;
	cds::Deque<size_t> d;

	for (size_t i = 1; i <= ntasks; ++i) {
		d.put(i);
	}

	ASSERT_EQ(d.size(), ntasks);

	for (size_t i = ntasks; i >= 1; --i) {
		size_t t = 0;
		ASSERT_TRUE(d.take(t));
		ASSERT_EQ(t, i);
	}

	size_t t;
	ASSERT_FALSE(d.take(t));
	ASSERT_FALSE(d.steal(t));
	ASSERT_EQ(d.size(), 0);
}

TEST(DequeTest, put_steal__single_no_resize) {
	size_t ntasks = 8;
	cds::Deque<size_t> d;

	for (size_t i = 1; i <= ntasks; ++i) {
		d.put(i);
	}

	ASSERT_EQ(d.size(), ntasks);

	for (size_t i = 1; i <= ntasks; ++i) {
		size_t t = 0;
		ASSERT_TRUE(d.steal(t));
		ASSERT_EQ(t, i);
	}

	size_t t;
	ASSERT_FALSE(d.take(t));
	ASSERT_FALSE(d.steal(t));
	ASSERT_EQ(d.size(), 0);
}

TEST(DequeTest, put_take__single_with_resize) {
	cds::Deque<size_t> d;
	size_t ntasks = 5000;

	ASSERT_LT(d.capacity(), ntasks);

	for (size_t i = 1; i <= ntasks; ++i)
		d.put(i);

	ASSERT_EQ(d.size(), ntasks);

	for (size_t i = ntasks; i >= 1; --i) {
		size_t t = 0;
		ASSERT_TRUE(d.take(t));
		ASSERT_EQ(t, i);
	}

	size_t t;
	ASSERT_FALSE(d.take(t));
	ASSERT_FALSE(d.steal(t));
	ASSERT_EQ(d.size(), 0);
}

TEST(DequeTest, put_steal__single_with_resize) {
	cds::Deque<size_t> d;
	size_t ntasks = 5000;

	ASSERT_LT(d.capacity(), ntasks);

	for (size_t i = 1; i <= ntasks; ++i) {
		d.put(i);
	}

	ASSERT_EQ(d.size(), ntasks);

	for (size_t i = 1; i <= ntasks; ++i) {
		size_t t = 0;
		ASSERT_TRUE(d.steal(t));
		ASSERT_EQ(t, i);
	}

	size_t t;
	ASSERT_FALSE(d.take(t));
	ASSERT_FALSE(d.steal(t));
	ASSERT_EQ(d.size(), 0);
}

TEST(DequeTest, put_steal__concurrent) {
	size_t ntasks = 1 << 14;

	std::vector<size_t> tasks;

	cds::Deque<size_t> d;

	for (size_t i = 1; i <= ntasks; ++i) {
		d.put(i);
		tasks.push_back(i);
	}

	size_t nthreads = std::thread::hardware_concurrency();

	std::atomic_size_t nready(0);
	std::atomic_bool stop(false);
	std::vector<size_t > stolen[nthreads];
	std::thread threads[nthreads];

	auto steal = [&](size_t id) {
		nready++;

		while (nready != nthreads)
			std::this_thread::yield();

		while (!stop) {
			size_t t;
			if (d.steal(t))
				stolen[id].push_back(t);
		}
	};

	for (size_t i = 0; i < nthreads; ++i) {
		threads[i] = std::thread(steal, i);
	}

	std::this_thread::sleep_for(std::chrono::seconds(3));
	stop = true;

	std::cerr << "total tasks: " << ntasks << "\n";
	for (size_t i = 0; i < nthreads; ++i) {
		threads[i].join();
		std::cerr << "thread #" << i << " tasks: " << stolen[i].size() << "\n";
	}

	for (size_t i = 0; i < nthreads; ++i) {
		for (auto t : stolen[i]) {
			auto found = std::find(tasks.begin(), tasks.end(), t);
			ASSERT_TRUE(found != tasks.end());
			tasks.erase(found);
		}
	}

	EXPECT_TRUE(tasks.empty());

	size_t t;
	ASSERT_FALSE(d.take(t));
	ASSERT_FALSE(d.steal(t));
	ASSERT_EQ(d.size(), 0);
}

TEST(DequeTest, put_steal_take__concurrent) {
	size_t ntasks = 1 << 14;

	cds::Deque<size_t> d;

	std::vector<size_t> tasks;

	for (size_t i = 1; i <= ntasks; ++i) {
		d.put(i);
		tasks.push_back(i);
	}

	size_t nthreads = std::thread::hardware_concurrency();

	std::atomic_size_t nready(0);
	std::atomic_bool stop(false);
	std::vector<size_t> stolen[nthreads];
	std::thread threads[nthreads];

	auto steal = [&](size_t id, bool steal) {
		nready++;

		while (nready != nthreads)
			std::this_thread::yield();

		while (!stop) {
			size_t t;

			if (steal ? d.steal(t) : d.take(t))
				stolen[id].push_back(t);
		}
	};

	for (size_t i = 0; i < nthreads; ++i) {
		threads[i] = std::thread(steal, i, i != 0);
	}

	std::this_thread::sleep_for(std::chrono::seconds(3));
	stop = true;

	std::cerr << "total tasks: " << ntasks << "\n";
	for (size_t i = 0; i < nthreads; ++i) {
		threads[i].join();
		std::cerr << "thread #" << i << " tasks: " << stolen[i].size() << "\n";
	}

	for (size_t i = 0; i < nthreads; ++i) {
		for (auto t : stolen[i]) {
			auto found = std::find(tasks.begin(), tasks.end(), t);
			ASSERT_TRUE(found != tasks.end());
			tasks.erase(found);
		}
	}

	EXPECT_TRUE(tasks.empty());

	size_t t;
	ASSERT_FALSE(d.take(t));
	ASSERT_FALSE(d.steal(t));
	ASSERT_EQ(d.size(), 0);
}

TEST(DequeTest, copy__deque_is_copied) {
	size_t ntasks = 8;
	cds::Deque<size_t> d1;

	for (size_t i = 1; i <= ntasks; ++i)
		d1.put(i);

	cds::Deque<size_t> d2 = d1;

	ASSERT_EQ(d1.size(), d2.size());

	for (size_t i = ntasks; i >= 1; --i) {
		size_t t = 0;
		ASSERT_TRUE(d2.take(t));
		ASSERT_EQ(t, i);
	}

	size_t t;
	ASSERT_FALSE(d2.take(t));
	ASSERT_FALSE(d2.steal(t));
	ASSERT_EQ(d2.size(), 0);
}

TEST(DequeTest, move__deque_is_move) {
	size_t ntasks = 8;
	cds::Deque<size_t> d1;

	for (size_t i = 1; i <= ntasks; ++i)
		d1.put(i);

	cds::Deque<size_t> d2 = std::move(d1);

	size_t t;
	ASSERT_FALSE(d1.take(t));
	ASSERT_FALSE(d1.steal(t));
	ASSERT_EQ(d1.size(), 0);

	ASSERT_EQ(d2.size(), ntasks);

	for (size_t i = ntasks; i >= 1; --i) {
		size_t t = 0;
		ASSERT_TRUE(d2.take(t));
		ASSERT_EQ(t, i);
	}
}
