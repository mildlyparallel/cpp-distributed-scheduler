
#include <iostream>
#include <cassert>
#include <random>

#include <mpi.h>

#include "BlockMatrix.hh"

#include "TaskInput.hh"
#include "TaskOutput.hh"
#include "Scheduler.hh"

using Matrix = BlockMatrix<int>;

class MatrixOperation {
public:
	enum Operation : unsigned {
		Add = 0,
		Multiply
	};

	class Input : public cds::TaskInput<Input> {
	public:
		Input()
		{ }

		Input(Operation op, Matrix *a, Matrix *b)
		: operation(op)
		, a(a)
		, b(b)
		{ }

		~Input() { }

		virtual void recv(int src) final { }

		virtual void send(int dst) const final { }

		Operation operation;
		Matrix *a;
		Matrix *b;
	};

	class Output : public cds::TaskOutput<Input, Output> {
	public:
		Output() { }

		~Output() { }

		virtual void run(
			cds::Worker<Input, Output> &worker,
			const Input &input
		) final;

		virtual void recv(int src) final { }

		virtual void send(int dst) const final { }

		Matrix result;
	};

	using Worker = cds::Worker<MatrixOperation::Input, MatrixOperation::Output>;

	static void add(
		Worker &worker,
		const Matrix &a,
		const Matrix &b,
		Matrix &c
	);

	static void multiply(
		Worker &worker,
		const Matrix &a,
		const Matrix &b,
		Matrix &c
	);
};

void MatrixOperation::add(
	MatrixOperation::Worker &worker,
	const Matrix &a,
	const Matrix &b,
	Matrix &c
) {
	assert(a.blocks() == b.blocks());
	assert(a.blocks() == c.blocks());
	assert(a.blocks() >= 1);

	if (a.blocks() == 1) {
		auto &ab = a.block(0, 0);
		auto &bb = a.block(0, 0);
		auto &cb = c.block(0, 0);

		cb.add(ab, bb);
		return;
	}

	auto ab = a.get_blocks();
	auto bb = b.get_blocks();

	Output output[4];


	for (size_t i = 0; i < 4; ++i) {
		output[i].result.shape(a.log_blocks() - 1);
		worker.spawn(&output[i], Add, &ab[i], &bb[i]);
	}

	worker.sync();

	for (size_t i = 0; i < 4; ++i)
		c.set_block(i, output[i].result);
}

void MatrixOperation::multiply(
	MatrixOperation::Worker &worker,
	const Matrix &a,
	const Matrix &b,
	Matrix &c
) {
	assert(a.blocks() == b.blocks());
	assert(a.blocks() == c.blocks());
	assert(a.blocks() >= 1);

	if (a.blocks() == 1) {
		auto &ab = a.block(0, 0);
		auto &bb = a.block(0, 0);
		auto &cb = c.block(0, 0);

		cb.multiply(ab, bb);
		return;
	}

/*      A     x     B     =         R         =     l       +     r    
 *  | 0 | 1 |   | 0 | 1 |   | 00+12 | 01+13 |   | 00 | 01 |   | 12 | 13 |
 *  |---+---| x |---+---| = |-------+-------| = |----+----| + |----+----|
 *  | 2 | 3 |   | 2 | 3 |   | 20+32 | 21+33 |   | 20 | 21 |   | 32 | 33 |
 */

	auto ab = a.get_blocks();
	auto bb = b.get_blocks();

	Output left[4];
	Output right[4];
	Output sum[4];

	for (size_t i = 0; i < 4; ++i) {
		left[i].result.shape(a.log_blocks() - 1);
		right[i].result.shape(a.log_blocks() - 1);
		sum[i].result.shape(a.log_blocks() - 1);
	}

	worker.spawn(&left[0], Multiply, &ab[0], &bb[0]);
	worker.spawn(&left[1], Multiply, &ab[0], &bb[1]);
	worker.spawn(&left[2], Multiply, &ab[2], &bb[0]);
	worker.spawn(&left[3], Multiply, &ab[2], &bb[1]);

	worker.spawn(&right[0], Multiply, &ab[1], &bb[2]);
	worker.spawn(&right[1], Multiply, &ab[1], &bb[3]);
	worker.spawn(&right[2], Multiply, &ab[3], &bb[2]);
	worker.spawn(&right[3], Multiply, &ab[3], &bb[3]);

	worker.sync();

	worker.spawn(&sum[0], Add, &left[0].result, &right[0].result);
	worker.spawn(&sum[1], Add, &left[1].result, &right[1].result);
	worker.spawn(&sum[2], Add, &left[2].result, &right[2].result);
	worker.spawn(&sum[3], Add, &left[3].result, &right[3].result);

	worker.sync();

	for (size_t i = 0; i < 4; ++i)
		c.set_block(i, sum[i].result);
}

void MatrixOperation::Output::run(
	MatrixOperation::Worker &worker,
	const MatrixOperation::Input &input
) {
	if (input.operation == Add) {
		add(worker, *input.a, *input.b, result);
	}

	if (input.operation == Multiply) {
		multiply(worker, *input.a, *input.b, result);
	}
}

int next_random() {
	static std::random_device rd;
	static std::default_random_engine gen(rd());
	static std::uniform_int_distribution<> distrib(-50, 50);
	return distrib(gen);
}

void print_size(std::ostream &os, size_t bytes)
{
	const char *unit[] = {"b", "kb", "mb", "gb"};

	for (size_t i = 0; i < 4; ++i) {
		if (bytes >= 1024 && i != 3) {
			bytes /= 1024;
			continue;
		}

		os << bytes << " " << unit[i];
		return;
	}
}

int main(int argc, char *argv[])
{
	MPI_Init(&argc, &argv);

	cds::Scheduler<MatrixOperation::Input, MatrixOperation::Output> scheduler;
	scheduler.set_mpi_comm(MPI_COMM_WORLD);
	// scheduler.set_nr_threads(1);

	scheduler.start();

	if (scheduler.get_proc_id() == 0) {
		Matrix a;
		Matrix b;

		size_t n = 6;

		a.shape(n);
		b.shape(n);

		a.fill_random(next_random);
		b.fill_random(next_random);

		size_t dim = a.dimention();
		std::cout << "Matrix dimentions = " << dim << " x " << dim << std::endl; 
		std::cout << "Matrix size = ";
		print_size(std::cout, dim * dim * sizeof(int));
		std::cout << std::endl;

		MatrixOperation::Output output;
		output.result.shape(n);


		scheduler.spawn(&output, MatrixOperation::Multiply, &a, &b);

		scheduler.sync();

	} else {
		scheduler.sync();
	}

	for (size_t i = 0; i < scheduler.get_nr_procs(); ++i) {
		if (i == scheduler.get_proc_id())
			scheduler.stop();
		MPI_Barrier(MPI_COMM_WORLD);
	}

	MPI_Finalize();

	return 0;
}
