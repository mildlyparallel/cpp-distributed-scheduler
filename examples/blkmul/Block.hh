#pragma once

#include <vector>
#include <cassert>

template <typename T>
class Block
{
public:
	Block();

	virtual ~Block();

	T &at(size_t i, size_t j);

	const T &at(size_t i, size_t j) const;

	void add(const Block<T> &other);

	void add(const Block<T> &a, const Block<T> &b);

	void multiply(const Block<T> &a, const Block<T> &b);

	template<typename F>
	void fill_random(F fn);

	static constexpr size_t log_dim = 4;
	static constexpr size_t dim = 1 << log_dim;

private:
	size_t get_id(size_t i, size_t j) const;

	static constexpr size_t m_size = dim * dim;

	T m_data[m_size];
};

template<typename T>
Block<T>::Block()
{
	std::fill(std::begin(m_data), std::end(m_data), 0);
}

template<typename T>
Block<T>::~Block()
{ }

template<typename T>
T &Block<T>::at(size_t i, size_t j)
{
	return m_data[get_id(i, j)];
}

template<typename T>
const T &Block<T>::at(size_t i, size_t j) const
{
	return m_data[get_id(i, j)];
}

template<typename T>
size_t Block<T>::get_id(size_t i, size_t j) const
{
	constexpr size_t bm = dim - 1;
	size_t oi = i & bm, oj = j & bm;
	size_t id = oi * dim + oj;
	assert(id < m_size);
	return id;
}

template<typename T>
void Block<T>::add(const Block<T> &other)
{
	for (size_t i = 0; i < m_size; ++i)
		m_data[i] += other.m_data[i];
}

template<typename T>
void Block<T>::add(const Block<T> &a, const Block<T> &b)
{
	for (size_t i = 0; i < m_size; ++i)
		m_data[i] = a.m_data[i] + b.m_data[i];
}

#if 0

template<typename T>
void Block<T>::multiply(const Block<T> &a, const Block<T> &b)
{
	constexpr size_t dim = Block<T>::dim;
	T tmp[dim];

	for (size_t i = 0; i < dim; ++i) {
		for (size_t j = 0; j < dim; ++j) {
			size_t nj = j;
			size_t ni = i * dim;
			size_t nij = ni + nj;

			for (size_t k = 0; k < dim; ++k) {
				tmp[k] = m_data[nj];
				nj += dim;
			}

			T r = 0;
			for (size_t k = 0; k < dim; ++k) {
				r += tmp[k] * m_data[ni];
				ni++;
			}

			m_data[nij] = r;
		}
	}
}

#else

template<typename T>
void Block<T>::multiply(const Block<T> &a, const Block<T> &b)
{
	constexpr size_t dim = Block<T>::dim;

	for (size_t i = 0; i < dim; ++i) {
		for (size_t j = 0; j < dim; ++j) {
			at(i, j) = 0;
			for (size_t k = 0; k < dim; ++k) {
				at(i, j) += a.at(k, j) * b.at(i, k);
			}
		}
	}
}

#endif

template<typename T>
template<typename F>
void Block<T>::fill_random(F fn)
{
	for (size_t i = 0; i < m_size; ++i)
		m_data[i] = fn();
}

