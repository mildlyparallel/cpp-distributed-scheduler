#pragma once

#include <vector>
#include <cassert>
#include <tuple>
#include <cstring>

#include "Block.hh"

template <typename T>
class BlockMatrix
{
public:
	BlockMatrix();

	virtual ~BlockMatrix();

	size_t dimention() const;

	size_t blocks() const;

	size_t log_blocks() const;

	T &at(size_t i, size_t j);

	const T &at(size_t i, size_t j) const;

	Block<T> &block(size_t i, size_t j);

	const Block<T> &block(size_t i, size_t j) const;

	void shape(size_t log_n);

	void resize(size_t n);

	template<typename F>
	void fill_random(F fn);

	std::array<BlockMatrix<T>, 4> get_blocks();

	std::array<BlockMatrix<T>, 4> get_blocks() const;

	void set_block(size_t i, const BlockMatrix<T> &other);

protected:

private:
	size_t block_id(size_t i, size_t j) const;

	static uint_fast64_t expand64(uint_fast64_t x);
	static uint_fast32_t expand32(uint_fast32_t x);

	BlockMatrix<T> get_view(size_t i) const;

	size_t m_ln;

	bool m_own;

	Block<T> *m_data;
};

template<typename T>
BlockMatrix<T>::BlockMatrix()
: m_ln(0)
, m_own(false)
, m_data(nullptr)
{ }

template<typename T>
BlockMatrix<T>::~BlockMatrix()
{
	if (m_own) {
		assert(m_data);
		delete []m_data;
	}
}

template<typename T>
void BlockMatrix<T>::set_block(size_t i, const BlockMatrix<T> &other)
{
	size_t ob = other.blocks();
	assert(blocks() == 2 * ob);
	assert(i < 4);

	std::memcpy(
		(void*)(m_data + i * ob),
		other.m_data,
		ob * sizeof(Block<T>)
	);
}

template<typename T>
void BlockMatrix<T>::shape(size_t log_n)
{
	assert(m_data == nullptr);

	m_ln = log_n;

	size_t sz = (1 << m_ln) * (1 << m_ln);

	m_data = new Block<T>[sz];
	m_own = true;
}

template<typename T>
size_t BlockMatrix<T>::blocks() const
{
	return 1 << m_ln;
}

template<typename T>
size_t BlockMatrix<T>::log_blocks() const
{
	return m_ln;
}

template<typename T>
size_t BlockMatrix<T>::dimention() const
{
	return blocks() * Block<T>::dim;
}

template<typename T>
T &BlockMatrix<T>::at(size_t i, size_t j)
{
	return block(i, j).at(i, j);
}

template<typename T>
const T &BlockMatrix<T>::at(size_t i, size_t j) const
{
	return block(i, j).at(i, j);
}

template<typename T>
Block<T> &BlockMatrix<T>::block(size_t i, size_t j)
{
	return m_data[block_id(i, j)];
}

template<typename T>
const Block<T> &BlockMatrix<T>::block(size_t i, size_t j) const
{
	return m_data[block_id(i, j)];
}

template<typename T>
uint_fast64_t BlockMatrix<T>::expand64(uint_fast64_t x)
{
	static const uint_fast64_t m[] = {
		0x00000000FFFFFFFF,
		0x0000FFFF0000FFFF,
		0x00FF00FF00FF00FF,
		0x0F0F0F0F0F0F0F0F,
		0x3333333333333333,
		0x5555555555555555
	};

	x = (x | x << 32) & m[0];
	x = (x | x << 16) & m[1];
	x = (x | x << 8)  & m[2];
	x = (x | x << 4)  & m[3];
	x = (x | x << 2)  & m[4];
	x = (x | x << 1)  & m[5];

	return x;
}

template<typename T>
uint_fast32_t BlockMatrix<T>::expand32(uint_fast32_t x)
{
	static const uint_fast32_t m[] = {
		0xFFFFFFFF,
		0x0000FFFF,
		0x00FF00FF,
		0x0F0F0F0F,
		0x33333333,
		0x55555555
	};

	x = (x | x << 16) & m[1];
	x = (x | x << 8)  & m[2];
	x = (x | x << 4)  & m[3];
	x = (x | x << 2)  & m[4];
	x = (x | x << 1)  & m[5];

	return x;
}

template<typename T>
size_t BlockMatrix<T>::block_id(size_t i, size_t j) const
{
	const size_t ld = Block<T>::log_dim;
	size_t bi = i >> ld, bj = j >> ld;
	size_t id = 0;

	if constexpr (sizeof(size_t) == 4)
		id = expand32(bj) | (expand32(bi) << 1);
	else 
		id = expand64(bj) | (expand64(bi) << 1);

	assert(id < blocks() * blocks());
	return id;
}

template<typename T>
BlockMatrix<T> BlockMatrix<T>::get_view(size_t i) const
{
	assert(i < 4);
	assert(blocks() > 1);
	size_t q = blocks() / 2;

	BlockMatrix<T> v;
	v.m_data = const_cast<Block<T> *>(m_data) + i * q;
	v.m_own = false;
	v.m_ln = m_ln - 1;
	return v;
}

template<typename T>
std::array<BlockMatrix<T>, 4> BlockMatrix<T>::get_blocks()
{
	return {
		get_view(0),
		get_view(1),
		get_view(2),
		get_view(3)
	};
}

template<typename T>
std::array<BlockMatrix<T>, 4> BlockMatrix<T>::get_blocks() const
{
	return {
		get_view(0),
		get_view(1),
		get_view(2),
		get_view(3)
	};
}

template<typename T>
template<typename F>
void BlockMatrix<T>::fill_random(F fn)
{
	size_t sz = blocks() * blocks();
	for (size_t i = 0; i < sz; ++i)
		m_data[i].fill_random(fn);
}
