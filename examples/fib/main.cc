#include <iostream>

#include <mpi.h>

#include "TaskInput.hh"
#include "TaskOutput.hh"
#include "Scheduler.hh"

using namespace cds;

constexpr unsigned long fib_seq(unsigned n) {
	if (n == 0)
		return 0;

	unsigned long f1 = 0;
	unsigned long f2 = 1;
	unsigned long s = 1;

	for (unsigned i = 2; i <= n; ++i) {
		s = f1 + f2;
		f1 = f2;
		f2 = s;
	}

	return s;
}

class FibInput : public TaskInput<FibInput> {
public:
	FibInput()
	: n(0)
	{  }

	FibInput(unsigned n)
	: n(n)
	{  }

	unsigned n;

	virtual void recv(int src) final {
		MPI_Recv(&n, 1, MPI_UNSIGNED, src, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	}

	virtual void send(int dst) const final {
		MPI_Send(&n, 1, MPI_UNSIGNED, dst, 0, MPI_COMM_WORLD);
	}
};

class FibOutput : public TaskOutput<FibInput, FibOutput> {
public:
	unsigned long ans;

	virtual void run(
		Worker<FibInput, FibOutput> &worker,
		const FibInput &input
	) final {
		if (input.n == 0) {
			ans = 1;
			return;
		}

		if (input.n <= 2) {
			ans = 1;
			return;
		}

		FibOutput o1;
		FibOutput o2;

		worker.spawn(&o1, input.n - 1);
		worker.spawn(&o2, input.n - 2);

		worker.sync();

		ans = o1.ans + o2.ans;
	}

	virtual void recv(int src) final {
		MPI_Recv(&ans, 1, MPI_UNSIGNED_LONG, src, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	}

	virtual void send(int dst) const final {
		MPI_Send(&ans, 1, MPI_UNSIGNED_LONG, dst, 0, MPI_COMM_WORLD);
	}
};

int main(int argc, char *argv[])
{
	MPI_Init(&argc, &argv);

	Scheduler<FibInput, FibOutput> scheduler;
	scheduler.set_mpi_comm(MPI_COMM_WORLD);
	// scheduler.set_nr_threads(1);

	scheduler.start();

	if (scheduler.get_proc_id() == 0) {
		unsigned input = 36;
		FibOutput output;

		scheduler.spawn(&output, input);

		scheduler.sync();

		std::cerr << "f(" << input << ") = " << output.ans;
		if (output.ans != fib_seq(input))
			std::cerr << " expected :" <<  fib_seq(input);
		std::cerr << std::endl;
	} else {
		scheduler.sync();
	}

	for (size_t i = 0; i < scheduler.get_nr_procs(); ++i) {
		if (i == scheduler.get_proc_id())
			scheduler.stop();
		MPI_Barrier(MPI_COMM_WORLD);
	}

	MPI_Finalize();
}
