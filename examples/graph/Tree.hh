#pragma once

#include <vector>
#include <iostream>
#include <random>
#include <fstream>
#include <cassert>

struct TreeNode
{
	size_t first_child;
	size_t last_child;
	size_t next_sibling;
	size_t prev_sibling;
	size_t nr_children;
};

class Tree
{
public:
	Tree() {
		m_tree.push_back({0, 0, 0});
		m_tree.push_back({0, 0, 0});
	}

	const TreeNode &at(size_t node_id) const {
		assert(node_id > 0);
		assert(node_id < m_tree.size());
		return m_tree[node_id];
	}

	size_t add_to(size_t root_id) {
		size_t node_id = m_tree.size();
		m_tree.push_back({0, 0, 0, 0});

		TreeNode &node = m_tree[node_id];

		TreeNode &root = m_tree[root_id];

		if (root.first_child == 0) {
			assert(root.last_child == 0);
			root.first_child = node_id;
			root.last_child = node_id;
			root.nr_children = 1;
			return node_id;
		}

		assert(root.last_child != 0);

		node.prev_sibling = root.last_child;

		TreeNode &last = m_tree[root.last_child];
		last.next_sibling = node_id;
		root.last_child = node_id;
		root.nr_children++;

		return node_id;
	}

	unsigned long fill_full(size_t depth, size_t breadth) {
		return fill_full(1, depth, breadth);
	}

	unsigned long fill_rnd(size_t depth, size_t breadth) {
		std::seed_seq seed{1};
		std::mt19937 eng(seed);

		return fill_rnd(1, depth, breadth, eng);
	}

	void print(const std::string &file) {
		std::ofstream out(file);

		out << R"AA(
	\documentclass[crop,tikz,multi=false]{standalone}
	\makeatletter
	\usepackage{tikz}
	\usetikzlibrary{graphdrawing}
	\usetikzlibrary{graphs}
	\usegdlibrary{trees}
	\begin{document}
	\begin{tikzpicture}[>=stealth, every node/.style={circle, draw, inner sep=2pt}]
	\graph [tree layout, grow=down, fresh nodes]
	{
	)AA";

		print(1, out);

		out << R"AA(
	};
	\end{tikzpicture}
	\end{document}
	)AA";
	}

private:
	void print(size_t root_id, std::ostream &os) {
		os << "{\"\"}" ;
		if (at(root_id).first_child == 0)
			return;

		os << " -> {";

		bool s = false;

		for (size_t i = at(root_id).first_child; i != 0; i = at(i).next_sibling) {
			if (s)
				os << ", ";

			s = true;

			print(i, os);
		}
		os << "}";
	}

	unsigned long fill_full(size_t root_id, size_t depth, size_t breadth) {
		if (depth == 0)
			return 1;

		unsigned long ret = 1;

		for (size_t i = 0; i < breadth; ++i) {
			size_t node_id = add_to(root_id);
			ret += fill_full(node_id, depth - 1, breadth);
		}

		return ret;
	}

	unsigned long fill_rnd(
		size_t root_id,
		size_t max_depth,
		size_t max_breadth,
		std::mt19937 &eng
	) {
		if (max_depth == 0)
			return 1;

		std::uniform_int_distribution<size_t> breadth_distr(1, max_breadth);
		std::uniform_int_distribution<size_t> depth_distr(0, max_depth - 1);
		
		size_t breadth = breadth_distr(eng);

		unsigned long ret = 1;

		for (size_t i = 0; i < breadth; ++i) {
			size_t node_id = add_to(root_id);
			ret += fill_rnd(node_id, depth_distr(eng), max_breadth, eng);
		}

		return ret;
	}

	std::vector<TreeNode> m_tree;
};

