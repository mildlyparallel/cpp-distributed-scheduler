#include <vector>
#include <iostream>
#include <fstream>
#include <cassert>

#include <mpi.h>

#include "TaskInput.hh"
#include "TaskOutput.hh"
#include "Scheduler.hh"

#include "Tree.hh"

#include "config.h"

#ifndef LOOP_ITER
#define LOOP_ITER 5e2
#endif

#ifndef NET_INPUT_SIZE
#define NET_INPUT_SIZE 64
#endif

#ifndef NET_INPUT_ITER
#define NET_INPUT_ITER 0
#endif

#ifndef NET_OUTPUT_SIZE
#define NET_OUTPUT_SIZE 64
#endif

#ifndef NET_OUTPUT_ITER
#define NET_OUTPUT_ITER 0
#endif

template <class T>
void do_not_opt_out(T &&x) {
	static auto ttid = std::this_thread::get_id();
	if (ttid == std::thread::id()) {
		const auto* p = &x;
		putchar(*reinterpret_cast<const char*>(p));
		std::abort();
	}
}

double sqrt_loop(std::default_random_engine &gen, size_t iter) {
	static std::uniform_real_distribution<double> distribution(0, 1);

	double s = 0;
	for (size_t i = 0; i < iter; ++i) {
		double rnd = distribution(gen);
		s += sqrt(rnd) * sqrt(rnd);
	}

	do_not_opt_out(s);

	return s;
}

Tree tree;

class TreeTaskInput : public cds::TaskInput<TreeTaskInput> {
	public:
		TreeTaskInput()
			: node_id(0)
		{  }

		TreeTaskInput(size_t node_id)
			: node_id(node_id)
			, rnd(0)
		{  }
		inline
			static thread_local std::default_random_engine generator;

		size_t node_id;
		double rnd;

		static constexpr size_t arr_size = NET_INPUT_SIZE / sizeof(int);
		int arr[arr_size];

		virtual void recv(int src) final {
			MPI_Recv(&node_id, 1, MPI_UNSIGNED_LONG, src, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

			rnd = sqrt_loop(generator, NET_INPUT_ITER);

			MPI_Recv(&rnd, 1, MPI_DOUBLE, src, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			MPI_Recv(arr, arr_size, MPI_INT, src, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		}

		virtual void send(int dst) const final {
			MPI_Send(&node_id, 1, MPI_UNSIGNED_LONG, dst, 0, MPI_COMM_WORLD);
			MPI_Send(&rnd, 1, MPI_DOUBLE, dst, 0, MPI_COMM_WORLD);
			MPI_Send(arr, arr_size, MPI_INT, dst, 0, MPI_COMM_WORLD);
		}
};

class TreeTaskOutput : public cds::TaskOutput<TreeTaskInput, TreeTaskOutput> {
	public:
		unsigned long ans;

		static constexpr size_t arr_size = NET_OUTPUT_SIZE / sizeof(int);
		double rnd;
		int arr[arr_size];

		inline
			static thread_local std::default_random_engine generator;

		virtual void run(
				cds::Worker<TreeTaskInput, TreeTaskOutput> &worker,
				const TreeTaskInput &input
				) final {
			const TreeNode &node = tree.at(input.node_id);

			if (node.nr_children == 0) {
				ans = 1;
				return;
			}

			std::vector<TreeTaskOutput> output(node.nr_children);

			rnd = input.rnd + sqrt_loop(generator, LOOP_ITER);

			// std::this_thread::sleep_for(std::chrono::milliseconds(SLEEP_MS));

			size_t j = 0;
			for (size_t i = node.first_child; i != 0; i = tree.at(i).next_sibling, ++j) {
				worker.spawn(&output[j], i);
			}

			worker.sync();

			ans = 1;
			for (auto &o : output) {
				ans += o.ans;
			}
		}

		virtual void recv(int src) final {
			MPI_Recv(&ans, 1, MPI_UNSIGNED_LONG, src, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

			rnd += sqrt_loop(generator, NET_OUTPUT_ITER);

			MPI_Recv(&rnd, 1, MPI_DOUBLE, src, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			MPI_Recv(arr, arr_size, MPI_INT, src, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		}

		virtual void send(int dst) const final {
			MPI_Send(&ans, 1, MPI_UNSIGNED_LONG, dst, 0, MPI_COMM_WORLD);
			MPI_Send(&rnd, 1, MPI_DOUBLE, dst, 0, MPI_COMM_WORLD);
			MPI_Send(arr, arr_size, MPI_INT, dst, 0, MPI_COMM_WORLD);
		}
};

int main(int argc, char *argv[])
{
	MPI_Init(&argc, &argv);

	size_t depth = 30;
	size_t breadth = 20;
	bool is_rnd = true;

	// size_t depth = 11;
	// size_t breadth = 4;
	// bool is_rnd = false;

	if (argc > 1)
		depth = std::atol(argv[1]);
	if (argc > 2)
		breadth = std::atol(argv[2]);
	if (argc > 3)
		is_rnd = false;


	unsigned long nr_nodes;
	if (is_rnd)
		nr_nodes = tree.fill_rnd(depth, breadth);
	else
		nr_nodes = tree.fill_full(depth, breadth);

	cds::Scheduler<TreeTaskInput, TreeTaskOutput> scheduler;
	scheduler.set_mpi_comm(MPI_COMM_WORLD);

	scheduler.start();

	if (scheduler.get_proc_id() == 0) {
		std::cerr << "depth = " << depth << std::endl;
		std::cerr << "breadth = " << breadth << std::endl;
		std::cerr << "is_rnd = " << is_rnd << std::endl;
		std::cerr << "nr_nodes = " << nr_nodes << std::endl;
	}

	if (scheduler.get_proc_id() == 0) {
		TreeTaskOutput output;
		auto st = std::chrono::high_resolution_clock::now();

		scheduler.spawn(&output, 1ul);

		scheduler.sync();
		auto en = std::chrono::high_resolution_clock::now();
		double dt = std::chrono::duration_cast<std::chrono::milliseconds>(en - st).count();

		std::cerr << "Time, sec = " << dt / 1e3 << std::endl;

		if (output.ans != nr_nodes) {
			std::cerr << "Expected " << nr_nodes << ", got " << output.ans << std::endl;
		} else {
			std::cerr << "Valid" << std::endl;
		}

	} else {
		scheduler.sync();
	}

	for (size_t i = 0; i < scheduler.get_nr_procs(); ++i) {
		if (i == scheduler.get_proc_id())
			scheduler.stop();
		MPI_Barrier(MPI_COMM_WORLD);
	}


	MPI_Finalize();
}

