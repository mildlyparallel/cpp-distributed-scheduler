#!/usr/bin/env Rscript

args <- commandArgs(trailingOnly = T)

nr_plots <- length(args)

png('steal-hist.png', width=600, height=nr_plots*250)

par(mfrow=c(nr_plots, 1))

for (i in 1:nr_plots) {
	data <- read.csv(args[i], header=T)

	y <- data$stolen / data$steals
	y[data$steals == 0] <- 1
	y[y < 0] <- 1
	y <- 1 - y

	d <- y[which(y > 0)]

	hist(
		d,
		main = args[i],
		breaks = 50,
		xlab = 'Steals ratio'
	)
}

dev.off()

