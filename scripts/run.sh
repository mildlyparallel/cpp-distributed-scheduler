#!/bin/bash

set -ex

function avg() {
	awk -F, "/^0,/{s+=\$$1; n++} END { print s/n }"
}

function max() {
	cut -d, -f$1 | sort -rg | head -n1
}

function min() {
	cut -d, -f$1 | sort -g | head -n1
}

# tres="0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1"
# tres="0.05 0.15 0.25 0.35 0.45 0.55 0.65 0.75 0.85 0.95"
# tres="$(seq 0.05 0.05 1)"
tres="$(seq 0.1 0.1 1)"
# tres="0.1 0.3 0.5 0.7 0.9 1"
bufs="4 8 32 128 256 512"
# bufs="128"
name=out6-rnd

[ -n $1 ] && name=$1

function run() {
	for b in $bufs
	do
		for t in $tres
		do
			d=${name}/$b/$t/

			[ -d $d ] && continue

			> config.h
			echo "#define STEAL_THRES $t" >> config.h
			echo "#define REQ_BUF_SIZE $b" >> config.h
			echo "#define LOOP_ITER 1e3" >> config.h
			echo "#define NET_INPUT_ITER 1e6" >> config.h
			echo "#define NET_OUTPUT_ITER 1e6" >> config.h
			echo "#define SAMPLING_RATE_US 1000" >> config.h
			echo "#define SAMPLING_RATE_TRACE_US 1000" >> config.h
			echo "#define ENABLE_TRACING 0" >> config.h

			rm -rf stats* total-* || true

			ninja ./examples/graph/main 
			mpirun -rf rf -np 4 ./examples/graph/main 2>&1 | tee output

			mkdir -p $d

			cp -t $d total-* stats* output || true
		done
	done
}

run

for b in $bufs
do
	col=$(realpath col${name}${b})
	echo $b > $col

	for t in $tres
	do
		d=${name}/$b/$t/

		if [ ! -d $d ]
		then 
			echo NA >> $col
			continue
		fi

		time=$(cat $d/output | awk '/sec =/{print $4}')
		echo $time >> $col
	done
done

printf "%s\n" time $tres > col0
paste -d',' col0 $(printf "col${name}%s " $bufs) > ${name}/time.dat
rm -rf col*

function stats_field() {
	field=$1

	for b in $bufs
	do
		col=$(realpath col${name}${b})
		echo $b > $col

		for t in $tres
		do
			d=${name}/$b/$t/

			if [ ! -d $d ]
			then 
				echo NA >> $col
				continue
			fi

			f="$(cat $d/total-stats-*.csv | head -n1 | tr , '\n' | grep -n $field | cut -d: -f1)"
			v="$(cat $d/total-stats-*.csv | grep '^0,' | avg $f)"
			echo $v >> $col
		done
	done

	printf "%s\n" $field $tres > col0
	paste -d',' col0 $(printf "col${name}%s " $bufs) > ${name}/field-$field.dat

	rm -rf col*
}

function stats_field_range() {
	field=$1

	for b in $bufs
	do
		col=$(realpath col${name}${b})
		echo $b > $col

		for t in $tres
		do
			d=${name}/$b/$t/

			if [ ! -d $d ]
			then 
				echo NA >> $col
				continue
			fi

			f="$(cat $d/total-stats-*.csv | head -n1 | tr , '\n' | grep -n $field | cut -d: -f1)"
			hi="$(cat $d/total-stats-*.csv | grep "^0," | max $f)"
			lo="$(cat $d/total-stats-*.csv | grep "^0," | min $f)"
			bc <<< "scale=6;$hi-$lo" >> $col
		done
	done

	printf "%s\n" $field $tres > col0
	paste -d',' col0 $(printf "col${name}%s " $bufs) > ${name}/field-$field-range.dat

	rm -rf col*
}

function stats_field_range_2() {
	field=$1

	for b in $bufs
	do
		col=$(realpath col${name}${b})
		echo $b > $col

		for t in $tres
		do
			d=${name}/$b/$t/

			if [ ! -d $d ]
			then 
				echo NA >> $col
				continue
			fi

			f="$(cat $d/total-stats-*.csv | head -n1 | tr , '\n' | grep -n $field | cut -d: -f1)"
			hi="$(cat $d/total-stats-*.csv | grep -v -e "^worker," -e "^0," | max $f)"
			lo="$(cat $d/total-stats-*.csv | grep -v -e "^worker," -e "^0," | min $f)"
			bc <<< "scale=6;$hi-$lo" >> $col
		done
	done

	printf "%s\n" $field $tres > col0
	paste -d',' col0 $(printf "col${name}%s " $bufs) > ${name}/field-$field-range.dat

	rm -rf col*
}

fields="ReqS ResOkS ResNoS"
for f in $fields
do
	stats_field $f
	stats_field_range $f
done

fields="Put Take TSteal OSteal"
for f in $fields
do
	stats_field_range_2 $f
done

